﻿using AspNet_4_MusicStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNet_4_MusicStore.Controllers
{
    public class HomeController : Controller
    {
        MusicStoreEntities dbContext = new MusicStoreEntities();

       
       public ActionResult Index()
       {
           SampleData.Seed(dbContext);
           return View();
       }

      
    }
}