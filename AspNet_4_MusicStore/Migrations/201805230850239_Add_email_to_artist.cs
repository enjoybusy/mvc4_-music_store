namespace AspNet_4_MusicStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_email_to_artist : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Artists", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Artists", "Email");
        }
    }
}
